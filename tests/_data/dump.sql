-- MySQL dump 10.13  Distrib 5.6.21, for osx10.9 (x86_64)
--
-- Host: localhost    Database: demo
-- ------------------------------------------------------
-- Server version	5.6.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `fortress_logins`
--

DROP TABLE IF EXISTS `fortress_logins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fortress_logins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `fortress_user_id` int(10) unsigned NOT NULL,
  `login_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fortress_logins_fortress_user_id_index` (`fortress_user_id`),
  CONSTRAINT `fortress_logins_fortress_user_id_foreign` FOREIGN KEY (`fortress_user_id`) REFERENCES `fortress_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fortress_logins`
--

LOCK TABLES `fortress_logins` WRITE;
/*!40000 ALTER TABLE `fortress_logins` DISABLE KEYS */;
/*!40000 ALTER TABLE `fortress_logins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fortress_role_user`
--

DROP TABLE IF EXISTS `fortress_role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fortress_role_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fortress_role_id` int(10) unsigned NOT NULL,
  `fortress_user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `fortress_role_user_fortress_role_id_index` (`fortress_role_id`),
  KEY `fortress_role_user_fortress_user_id_index` (`fortress_user_id`),
  CONSTRAINT `fortress_role_user_fortress_role_id_foreign` FOREIGN KEY (`fortress_role_id`) REFERENCES `fortress_roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fortress_role_user_fortress_user_id_foreign` FOREIGN KEY (`fortress_user_id`) REFERENCES `fortress_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fortress_role_user`
--

LOCK TABLES `fortress_role_user` WRITE;
/*!40000 ALTER TABLE `fortress_role_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `fortress_role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fortress_roles`
--

DROP TABLE IF EXISTS `fortress_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fortress_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `fortress_roles_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fortress_roles`
--

LOCK TABLES `fortress_roles` WRITE;
/*!40000 ALTER TABLE `fortress_roles` DISABLE KEYS */;
INSERT INTO `fortress_roles` VALUES (1,'user.admin','[\"user.admin\"]','2015-10-19 17:46:24','2015-10-19 17:47:29'),(2,'user.changePassword','[\"user.changePassword\"]','2015-10-19 17:46:33','2015-10-19 17:47:22'),(3,'user.bulkUpload','[\"user.bulkUpload\"]','2015-10-19 17:46:46','2015-10-19 17:47:26'),(4,'user.loginAsUser','[\"user.loginAsUser\"]','2015-10-19 17:46:46','2015-10-19 17:47:26'),(5,'role.admin','[\"role.admin\"]','2015-10-19 17:47:15','2015-10-19 17:47:39');
/*!40000 ALTER TABLE `fortress_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fortress_throttles`
--

DROP TABLE IF EXISTS `fortress_throttles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fortress_throttles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `fortress_user_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `suspended` tinyint(1) NOT NULL DEFAULT '0',
  `suspended_at` timestamp NULL DEFAULT NULL,
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `banned_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fortress_throttles_fortress_user_id_index` (`fortress_user_id`),
  CONSTRAINT `fortress_throttles_fortress_user_id_foreign` FOREIGN KEY (`fortress_user_id`) REFERENCES `fortress_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fortress_throttles`
--

LOCK TABLES `fortress_throttles` WRITE;
/*!40000 ALTER TABLE `fortress_throttles` DISABLE KEYS */;
/*!40000 ALTER TABLE `fortress_throttles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fortress_timezones`
--

DROP TABLE IF EXISTS `fortress_timezones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fortress_timezones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `country_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `coordinates` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timezone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fortress_timezones_country_code_index` (`country_code`),
  KEY `fortress_timezones_timezone_index` (`timezone`)
) ENGINE=InnoDB AUTO_INCREMENT=416 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fortress_timezones`
--

LOCK TABLES `fortress_timezones` WRITE;
/*!40000 ALTER TABLE `fortress_timezones` DISABLE KEYS */;
/*!40000 ALTER TABLE `fortress_timezones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fortress_users`
--

DROP TABLE IF EXISTS `fortress_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fortress_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `middle_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fortress_timezone_id` int(10) unsigned DEFAULT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  `activation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activated_at` timestamp NULL DEFAULT NULL,
  `persist_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_super_user` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `fortress_users_username_unique` (`username`),
  UNIQUE KEY `fortress_users_email_unique` (`email`),
  KEY `fortress_users_activation_code_index` (`activation_code`),
  KEY `fortress_users_reset_password_code_index` (`reset_password_code`),
  KEY `fortress_users_fortress_timezone_id_foreign` (`fortress_timezone_id`),
  CONSTRAINT `fortress_users_fortress_timezone_id_foreign` FOREIGN KEY (`fortress_timezone_id`) REFERENCES `fortress_timezones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fortress_users`
--

LOCK TABLES `fortress_users` WRITE;
/*!40000 ALTER TABLE `fortress_users` DISABLE KEYS */;
INSERT INTO `fortress_users` VALUES (1,'2015-10-13 18:16:39','2015-10-13 18:16:39','admin','$2y$10$QZlfhzH.ohEJ/q8V8y8H4OTAuTQ2Pd8TOfgpRK0wp3CPHyffXx2iG','','','',NULL,NULL,1,NULL,'2015-10-13 18:16:39',NULL,NULL,1,NULL);
/*!40000 ALTER TABLE `fortress_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2015_02_20_000001_create_fortress_timezones_table',1),('2015_02_20_000002_seed_fortress_timezones_table',1),('2015_02_20_000003_create_fortress_users_table',1),('2015_02_20_000004_create_fortress_groups_table',1),('2015_02_20_000005_create_fortress_groups_users_table',1),('2015_02_20_000006_create_fortress_throttles_table',1),('2015_03_12_000001_create_fortress_logins_table',1),('2015_09_05_180223_remove_old_tables',1),('2015_09_05_181000_alter_user_table',1),('2015_09_06_180422_create_roles_table',1),('2015_09_06_235701_create_role_user_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-10-13 10:20:19
