<?php

use Mockery as m;
use Cccisd\Fortress\Fortress;
use Cccisd\Fortress\Models\User;
use Cccisd\Fortress\UserNotFoundException;

class FortressTest extends \TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->mock = m::mock('Eloquent');
        $this->fortress = new Fortress(
            $this->user = m::mock('Cccisd\Fortress\Models\User')
        );
    }


    public function tearDown() {
        m::close();
    }


    /**
     * @return void
     */
    public function testRegister()
    {
        $credentials = array(
            'email'    => 'foo@bar.com',
            'password' => 'sdf_sdf',
        );

        $user = m::mock('Cccisd\Fortress\Models\User');
        $user->shouldReceive('setAttribute')->once();
        $user->shouldReceive('save')->once();

        $this->user->shouldReceive('newInstance')->with($credentials)->once()->andReturn($user);
        $this->assertEquals($user, $this->fortress->register($credentials, false));
    }


    /**
     * @return void
     */
    public function testRegisterWithActivation()
    {
        $credentials = array(
            'email'    => 'foo@bar.com',
            'password' => 'sdf_sdf',
        );

        $user = m::mock('Cccisd\Fortress\Models\User');
        $user->shouldReceive('setAttribute')->once();
        $user->shouldReceive('save')->once();
        $user->shouldReceive('getActivationCode')->once()->andReturn('activation_code_here');
        $user->shouldReceive('attemptActivation')->with('activation_code_here')->once();

        $this->user->shouldReceive('newInstance')->with($credentials)->once()->andReturn($user);
        $this->assertEquals($user, $this->fortress->register($credentials, true));
    }


    /**
     * @expectedException Cccisd\Fortress\UserNotFoundException
     */
    public function testAuthenticatingUserWhereTheUserDoesNotExist()
    {
        $credentials = array(
            'email'    => 'foo@bar.com',
            'password' => 'baz_bat',
        );

        $this->user->shouldReceive('findByCredentials')->with($credentials)->once()->andThrow(new UserNotFoundException);
        $this->fortress->authenticate($credentials);
    }


    public function testAuthenticatingUser()
    {
        $this->fortress = m::mock('Cccisd\Fortress\Fortress[login]', array($this->user));

        $credentials = array(
            'email'    => 'foo@bar.com',
            'password' => 'baz_bat',
        );

        $user = m::mock('Cccisd\Fortress\Models\User');
        $this->user->shouldReceive('findByCredentials')->with($credentials)->once()->andReturn($user);
        $this->fortress->shouldReceive('login')->with($user, false)->once();
        $this->fortress->authenticate($credentials);
    }


    /**
     * @expectedException Cccisd\Fortress\UserNotActivatedException
     */
    public function testLoginUserWhereHeIsNotActivated()
    {
        $user = m::mock('Cccisd\Fortress\Models\User');
        $user->shouldReceive('isActivated')->once()->andReturn(false);
        $user->shouldReceive('getLogin')->once();

        $this->fortress->login($user);
    }


    public function testLoginUser()
    {
        $this->fortress = m::mock('Cccisd\Fortress\Fortress[getAttribute]', array($this->user));

        $user = m::mock('Cccisd\Fortress\Models\User');
        $user->shouldReceive('isActivated')->once()->andReturn(true);
        $user->shouldReceive('getId')->once()->andReturn('foo');
        $user->shouldReceive('getPersistCode')->once()->andReturn('persist_code');
        $user->shouldReceive('recordLogin')->once();

        \Session::shouldReceive('put')->once()->with(\Config::get('fortress::session.key'), array('foo', 'persist_code'));
        \Cookie::shouldReceive('forever')->never();

        $this->fortress->login($user);
    }


    public function testLoginUserAndRemember()
    {
        $this->fortress = m::mock('Cccisd\Fortress\Fortress[getAttribute]', array($this->user));

        $user = m::mock('Cccisd\Fortress\Models\User');
        $user->shouldReceive('isActivated')->once()->andReturn(true);
        $user->shouldReceive('getId')->once()->andReturn('foo');
        $user->shouldReceive('getPersistCode')->once()->andReturn('persist_code');
        $user->shouldReceive('recordLogin')->once();

        \Session::shouldReceive('put')->once()->with(\Config::get('fortress::session.key'), array('foo', 'persist_code'));
        \Cookie::shouldReceive('forever')->once()->with(\Config::get('fortress::session.key'), array('foo', 'persist_code'));

        $this->fortress->login($user, true);
    }


    /**
     * @expectedException \BadMethodCallException
     */
    public function testHasAccessThrowBadMethodCallException()
    {
        $this->fortress->badMethod();
    }
}
