<?php
use \AcceptanceTester;

class ManageRolesCest
{
    public function _before(AcceptanceTester $I)
    {
        // login as admin
        $I->amOnPage('fortress/admin');
        $I->seeInCurrentUrl('fortress/login');
        $I->fillField('username', 'admin');
        $I->fillField('password', '123456');
        $I->click('Log in');
        $I->see('User administration');
        $I->click('Roles');
        $I->waitForText('Add role', 2, 'button');
    }

    public function _after(AcceptanceTester $I)
    {
    }
}
