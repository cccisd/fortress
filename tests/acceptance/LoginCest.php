<?php
use \AcceptanceTester;

class LoginCest
{
    public function _before(AcceptanceTester $I)
    {
        $I->amOnPage('fortress/admin');
        $I->seeInCurrentUrl('fortress/login');
    }

    public function _after(AcceptanceTester $I)
    {
    }

    public function loginWithNoCredentials(AcceptanceTester $I)
    {
        $I->click('Log in');
        $I->see('The username field is required.');
        $I->see('The password field is required.');
    }


    public function loginWithWrongUsername(AcceptanceTester $I)
    {
        $I->fillField('username', 'wrong_username');
        $I->fillField('password', 'wrong_password');
        $I->click('Log in');
        $I->see('A user was not found with the given credentials.');
    }


    public function loginWithWrongPassword(AcceptanceTester $I)
    {
        $I->fillField('username', 'admin');
        $I->fillField('password', 'wrong_password');
        $I->click('Log in');
        $I->see('The password is incorrect');
    }


    public function loginWithCorrectCredentials(AcceptanceTester $I)
    {
        $I->fillField('username', 'admin');
        $I->fillField('password', '123456');
        $I->click('Log in');
        $I->see('User administration');
        $I->see('admin', '#user_login_attribute');
        $I->dontSeeCookie('cccisd_fortress');
        $I->seeNumRecords(1, 'fortress_logins', ['fortress_user_id' => 1]);

        // Logout
        $I->click('Logout');
        $I->seeInCurrentUrl('fortress/login');
        $I->see('Login', 'h1');
    }


    public function loginAndSeeDefaultPage(AcceptanceTester $I)
    {
        $I->amOnPage('fortress/logout');
        $I->seeInCurrentUrl('fortress/login');
        $I->fillField('username', 'admin');
        $I->fillField('password', '123456');
        $I->click('Log in');

        $I->see('Default Page');
    }


    public function loginWithRememberMe(AcceptanceTester $I)
    {
        $I->fillField('username', 'admin');
        $I->fillField('password', '123456');
        $I->checkOption('#remember_me');
        $I->click('Log in');
        $I->see('User administration');
        $I->seeCookie('laravel_session');
        $I->seeCookie('cccisd_fortress');

        // Logout
        $I->click('Logout');
        $I->seeInCurrentUrl('fortress/login');
        $I->dontSeeCookie('cccisd_fortress');
    }
}
