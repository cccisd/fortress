<?php
use \AcceptanceTester;

class RegisterCest
{
    public function _before(AcceptanceTester $I)
    {
        $I->amOnPage('fortress/register');
    }

    public function _after(AcceptanceTester $I)
    {
    }

    public function registerWithIncorrectUsername(AcceptanceTester $I)
    {
        $I->fillField('username', '#*$^*#^*^#*');
        $I->click('.btn.btn-primary');
        $I->see('The username format is invalid.');
    }


    public function registerWithCorrectUsername(AcceptanceTester $I)
    {
        $I->fillField('username', 'asjhHDSKHDKS-_@.');
        $I->click('.btn.btn-primary');
        $I->dontSee('The username format is invalid.');
    }


    public function checkDefaultPage(AcceptanceTester $I)
    {
        $username = 'person';
        $password = '123456';

        $I->fillField('username', $username);
        $I->fillField('password', $password);
        $I->fillField('password_repeat', $password);
        $I->click('.btn.btn-primary');
        $I->see('Default Page');
        $I->see($username, '#user_login_attribute');
    }
}
