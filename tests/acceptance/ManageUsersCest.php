<?php
use \AcceptanceTester;

class ManageUsersCest
{
    protected $username = 'test';
    protected $password = '123456';

    public function _before(AcceptanceTester $I)
    {
        // login as admin
        $I->amOnPage('fortress/admin');
        $I->seeInCurrentUrl('fortress/login');
        $I->fillField('username', 'admin');
        $I->fillField('password', '123456');
        $I->click('Log in');
        $I->see('User administration');
    }

    public function _after(AcceptanceTester $I)
    {
    }


    public function addUserWithoutData(AcceptanceTester $I)
    {
        $I->click('Add user');
        $I->waitForElement('.modal.fade.in');
        $I->see('Add user', 'h4');

        $I->click('.modal.fade.in button[type=submit]');
        $I->waitForText('The username field is required.', 2);
        $I->waitForText('The password field is required.', 2);
    }


    public function addUserWithTheSameUsername(AcceptanceTester $I)
    {
        $I->click('Add user');
        $I->waitForElement('.modal.fade.in');
        $I->see('Add user', 'h4');

        $I->fillField('username', 'admin');
        $I->fillField('password', 'something');
        $I->click('.modal.fade.in button[type=submit]');
        $I->waitForText('The username has already been taken.', 2);
        $I->waitForText('The password and password repeat must match.', 2);
    }


    public function addUser(AcceptanceTester $I)
    {
        $password = '123456';
        $data = [
            'username' => 'test',
            'email' => 'fake@google.com',
            'first_name' => 'Bob',
            'last_name' => 'Dilon',
        ];
        $newData = [
            'username' => 'testNew',
            'email' => 'fakeNew@google.com',
            'first_name' => 'BobNew',
            'last_name' => 'DilonNew',
        ];


        // Check that there is no test user
        $I->dontSee($data['username'], 'td');
        $I->dontSee($data['email'], 'td');
        $I->dontSee($data['first_name'].' '.$data['last_name'], 'td');

        $I->click('Add user');
        $I->waitForElement('.modal.fade.in');
        $I->see('Add user', 'h4');
        $I->see('user.admin', 'label');
        $I->see('role.admin', 'label');

        // Add new user
        $I->submitForm('.modal.fade.in form', $data);
        $I->fillField('password', $password);
        $I->fillField('password_repeat', $password);
        $I->click('.modal.fade.in button[type=submit]');
        $I->waitForElementNotVisible('.modal.fade.in', 2);
        $I->dontSee('Add user', 'h4');
        $I->see($data['username'], 'td');
        $I->see($data['email'], 'td');
        $I->see($data['first_name'].' '.$data['last_name'], 'td');

        // Edit this user
        $I->click('#edit_user_button_2');
        $I->waitForElement('.modal.fade.in');
        $I->seeInFormFields('.modal.fade.in form', $data);
        // Try to change username to admin
        $I->fillField('username', 'admin');
        $I->click('.modal.fade.in button[type=submit]');
        $I->waitForText('The username has already been taken.', 2);
        // Update with real new data
        $I->submitForm('.modal.fade.in form', $newData);
        $I->waitForElementNotVisible('.modal.fade.in', 2);
        $I->see($newData['username'], 'td');
        $I->see($newData['email'], 'td');
        $I->see($newData['first_name'].' '.$newData['last_name'], 'td');
    }


    public function addNotSuperUser(AcceptanceTester $I)
    {
        $username = 'notSuperUser';
        $password = 'password';

        // Try add user without data
        $I->click('Add user');
        $I->waitForElement('.modal.fade.in');
        $I->fillField('username', $username);
        $I->fillField('password', $password);
        $I->fillField('password_repeat', $password);
        $I->click('.modal.fade.in button[type=submit]');
        $I->waitForElementNotVisible('.modal.fade.in', 2);
        // Check is_super_user in database
        $I->seeInDatabase('fortress_users', array('username' => $username, 'is_super_user' => 0));

        // Change super_user
        $I->click('#edit_user_button_2');
        $I->waitForElement('.modal.fade.in');
        $I->checkOption('#is_super_user');
        $I->click('.modal.fade.in button[type=submit]');
        $I->waitForElementNotVisible('.modal.fade.in', 2);
        // Check is_super_user in database
        $I->seeInDatabase('fortress_users', array('username' => $username, 'is_super_user' => 1));

        // Delete this user
        $I->see($username, 'td');
        $I->click('.delete_user_button_2');
        $I->acceptPopup();
        $I->waitForElementNotVisible('.delete_user_button_2', 2);
        $I->dontSee($username, 'td');
    }


    public function addSuperUser(AcceptanceTester $I)
    {
        $username = 'notSuperUser';
        $password = 'password';

        // Try add user without data
        $I->click('Add user');
        $I->waitForElement('.modal.fade.in');
        $I->fillField('username', $username);
        $I->fillField('password', $password);
        $I->fillField('password_repeat', $password);
        $I->checkOption('#is_super_user');
        $I->click('.modal.fade.in button[type=submit]');
        $I->waitForElementNotVisible('.modal.fade.in', 2);
        // Check is_super_user in database
        $I->seeInDatabase('fortress_users', array('username' => $username, 'is_super_user' => 1));

        // Change super_user
        $I->click('#edit_user_button_2');
        $I->waitForElement('.modal.fade.in');
        $I->uncheckOption('#is_super_user');
        $I->click('.modal.fade.in button[type=submit]');
        $I->waitForElementNotVisible('.modal.fade.in', 2);
        // Check is_super_user in database
        $I->seeInDatabase('fortress_users', array('username' => $username, 'is_super_user' => 0));
    }


    public function addUserAndLoginAsHim(AcceptanceTester $I)
    {
        $username = 'test';
        $password = '123456';

        // Try add user without data
        $I->click('Add user');
        $I->waitForElement('.modal.fade.in');
        $I->fillField('username', $username);
        $I->fillField('password', $password);
        $I->fillField('password_repeat', $password);
        $I->checkOption('#is_super_user');
        $I->click('.modal.fade.in button[type=submit]');
        $I->waitForElementNotVisible('.modal.fade.in', 2);

        // Logout
        $I->click('Logout');
        $I->seeInCurrentUrl('fortress/login');
        $I->see('Login', 'h1');

        // login as this user
        $I->amOnPage('fortress/admin');
        $I->fillField('username', $username);
        $I->fillField('password', $password);
        $I->click('Log in');
        $I->see('User administration');
        $I->see($username, '#user_login_attribute');
    }


    public function changePasswordWithNoData(AcceptanceTester $I)
    {
        $password = '123456';

        $I->click('#change_password_button_1');
        $I->waitForElement('.modal.fade.in');
        $I->click('.modal.fade.in button[type=submit]');
        $I->waitForText('The password field is required.', 2);
    }


    public function changePasswordWithNoRepeat(AcceptanceTester $I)
    {
        $password = '123456';

        $I->click('#change_password_button_1');
        $I->waitForElement('.modal.fade.in');
        $I->fillField('password', $password);
        $I->click('.modal.fade.in button[type=submit]');
        $I->waitForText('The password and password repeat must match.', 2);
    }


    public function changePassword(AcceptanceTester $I)
    {
        $password = 'newPassword';

        $I->click('#change_password_button_1');
        $I->waitForElement('.modal.fade.in');
        $I->fillField('password', $password);
        $I->fillField('password_repeat', $password);
        $I->click('.modal.fade.in button[type=submit]');
        $I->waitForElementNotVisible('.modal.fade.in', 2);

        // Logout
        $I->click('Logout');
        $I->seeInCurrentUrl('fortress/login');
        $I->see('Login', 'h1');

        // login as this user
        $I->amOnPage('fortress/admin');
        $I->fillField('username', 'admin');
        $I->fillField('password', $password);
        $I->click('Log in');
        $I->see('User administration');
    }


    public function loginAsUser(AcceptanceTester $I)
    {
        $this->addUserWithRoles($I, ['1', '1']);
        $I->seeElement('#login_as_user_button_2');
        $I->dontSee('Go back to "admin" user');

        $I->click('#login_as_user_button_2');
        $I->see('Default Page');
        $I->see('Go back to "admin" user');

        $I->amOnPage('fortress/admin');
        $I->see('User administration');
        $I->dontSee('Bulk Upload', 'button');
        $I->dontSee('Roles', 'li');
        $I->dontSeeElement('#change_password_button_1');
        $I->dontSeeElement('#login_as_user_button_1');
        $I->see('Go back to "admin" user');

        $I->click('Go back to "admin" user');
        $I->see('User administration');
        $I->see('Bulk Upload', 'button');
        $I->see('Roles', 'li');
        $I->seeElement('#change_password_button_1');
        $I->seeElement('#login_as_user_button_1');
        $I->dontSee('Go back to "admin" user');
    }


    public function checkNoPermissions(AcceptanceTester $I)
    {
        $username = 'test';
        $password = 'password';

        // Try add user without data
        $I->click('Add user');
        $I->waitForElement('.modal.fade.in');
        $I->submitForm('#add_user_form', [
            'username' => $username,
            'password' => $password,
            'password_repeat' => $password,
        ]);
        $I->waitForElementNotVisible('.modal.fade.in', 2);

        $I->click('Logout');
        $I->amOnPage('fortress/admin');
        $I->fillField('username', $username);
        $I->fillField('password', $password);
        $I->click('Log in');
        $I->see('HttpException');
    }


    public function checkUserAdminPermissions(AcceptanceTester $I)
    {
        $this->addUserWithRoles($I, ['1']);

        $I->click('Logout');
        $this->loginTestUser($I);
        $I->see('User administration');
        $I->dontSee('Bulk Upload', 'button');
        $I->dontSee('Roles', 'li');
        $I->dontSeeElement('#change_password_button_1');
        $I->dontSeeElement('#login_as_user_button_1');
    }


    public function checkUserChangePasswordPermissions(AcceptanceTester $I)
    {
        $this->addUserWithRoles($I, ['1', '2']);

        $I->click('Logout');
        $this->loginTestUser($I);
        $I->see('User administration');
        $I->dontSee('Bulk Upload', 'button');
        $I->dontSee('Roles', 'li');
        $I->seeElement('#change_password_button_1');
        $I->dontSeeElement('#login_as_user_button_1');

        $I->click('#change_password_button_1');
        $I->waitForElement('.modal.fade.in');
        $I->see('Change Password', 'h4');
    }


    public function checkUserBulkUploadPermissions(AcceptanceTester $I)
    {
        $this->addUserWithRoles($I, ['1', '3']);

        $I->click('Logout');
        $this->loginTestUser($I);
        $I->see('User administration');
        $I->see('Bulk Upload', 'button');
        $I->dontSee('Roles', 'li');
        $I->dontSeeElement('#change_password_button_1');
        $I->dontSeeElement('#login_as_user_button_1');

        $I->click('Bulk Upload');
        $I->waitForElement('.modal.fade.in');
        $I->see('User Bulk Upload', 'h4');
    }


    public function checkUserLoginAsUserPermissions(AcceptanceTester $I)
    {
        $this->addUserWithRoles($I, ['1', '4']);

        $I->click('Logout');
        $this->loginTestUser($I);
        $I->see('User administration');
        $I->dontSee('Bulk Upload', 'button');
        $I->dontSee('Roles', 'li');
        $I->dontSeeElement('#change_password_button_1');
        $I->seeElement('#login_as_user_button_1');

        $I->click('#login_as_user_button_1');
        $I->see('Default Page');
        $I->see('Go back to "'.$this->username.'" user');
    }


    public function checkRoleAdminPermissions(AcceptanceTester $I)
    {
        $this->addUserWithRoles($I, ['1', '5']);

        $I->click('Logout');
        $this->loginTestUser($I);
        $I->see('User administration');
        $I->dontSee('Bulk Upload', 'button');
        $I->see('Roles', 'li');
        $I->dontSee('#change_password_button_1');

        $I->click('Roles');
        $I->waitForText('Add role', 2, 'button');
    }


//-----------------------------------------------------------------------------
//----------------------- Protected helper functions --------------------------
//-----------------------------------------------------------------------------


    protected function addUserWithRoles($I, $roles = [])
    {
        $I->click('Add user');
        $I->waitForElement('.modal.fade.in');
        $I->submitForm('#add_user_form', [
            'username' => $this->username,
            'password' => $this->password,
            'password_repeat' => $this->password,
            'roles' => $roles,
        ]);
        $I->waitForElementNotVisible('.modal.fade.in', 2);
    }


    protected function loginTestUser($I)
    {
        $I->amOnPage('fortress/admin');
        $I->fillField('username', $this->username);
        $I->fillField('password', $this->password);
        $I->click('Log in');
    }
}
