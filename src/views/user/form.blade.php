{{ form_start($form) }}


<div class="row">
    <div class="col-xs-6">{{ form_row($form->username) }}</div>
</div>


{{ form_row($form->email) }}


<div class="row">
    <div class="col-xs-6">{{ form_row($form->first_name) }}</div>
    <div class="col-xs-6">{{ form_row($form->last_name) }}</div>
</div>


@if ($form->password)
<div class="row">
    <div class="col-xs-6">{{ form_row($form->password) }}</div>
</div>

<div class="row">
    <div class="col-xs-6">{{ form_row($form->password_repeat) }}</div>
</div>
@endif


@if (\Fortress::isSuperUser())
    {{ form_row($form->is_super_user) }}
@endif


<div class="form-group">
    <label>Roles</label>

    @if (count($roles) > 0)
        <div class="checkbox" style="margin-top: 0;">
            @foreach ($roles as $id => $role)
                <label>
                    {{ Form::checkbox('roles[]', $id, isset($userRoles[$id])) }}
                    {{ $role }}
                </label><br>
            @endforeach
        </div>
    @else
        <div>There are no roles to apply yet</div>
    @endif
</div>


{{ form_row($form->submit)}}


{{ form_end($form, false) }}
