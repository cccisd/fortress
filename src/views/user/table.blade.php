@extends('fortress::layouts.base')
@section('fortress.body')
    <script>
        var Fortress = Fortress || {};
        Fortress.userBulkColumns = {{ json_encode(\Fortress::getUserInstance()->getBulkColumns()) }};
        Fortress.roles = {{ json_encode(\Cccisd\Fortress\Models\Role::all()) }};
    </script>

    <div class="container">
        <div class="target"></div>
    </div>
@stop