@extends(\Config::get('fortress::layout.extends'))
@section(\Config::get('fortress::layout.section'))

{{ HTML::style('packages/cccisd/fortress/build/css/app.css') }}

<script>
    var Fortress = {
        publicUrl: '{{ URL::to('/').'/' }}',
        baseUrl: '{{ URL::to('/').'/'.Config::get('fortress::routes.prefix').'/' }}',
        permissions: {{ json_encode(array_keys(\Fortress::getActualPermissions())) }},
        config: {{ json_encode(\Config::getItems()['fortress::config']) }},
        hasAccess: function(permission) {
            if (this.permissions.indexOf(permission) === -1) {
                return false;
            }

            return true;
        }
    };
</script>

@section('fortress.body')
@show

{{ HTML::script('packages/cccisd/fortress/build/js/vendor.js') }}
{{ HTML::script('packages/cccisd/fortress/build/js/app.js') }}

@stop
