<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    {{ HTML::style('//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css') }}
</head>
<body data-route="{{ \Route::currentRouteAction() }}">
    <div class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <a href="" class="navbar-brand">Fortress</a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-left">
                    @if (\Fortress::check() && \Fortress::hasAccess('user.admin'))
                        <li>{{ link_to_route('fortress.admin', 'Admin area'); }}</li>
                    @endif
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    @if (\Fortress::check())
                        @if ($originalUser = \Fortress::getOriginalUser())
                            <li>
                                <a href="{{ route('fortress.admin.user.logoutAsUser'); }}">
                                    <span class="glyphicon glyphicon-arrow-left"></span>
                                    Go back to "{{ $originalUser->{\Config::get('fortress::user.login_attribute')} }}" user
                                </a>
                            </li>
                        @endif
                        <li>
                            <a href="#" id="user_login_attribute">
                                <span class="glyphicon glyphicon-user"></span>
                                {{ \Fortress::getUser()->{\Config::get('fortress::user.login_attribute')} }}
                            </a>
                        </li>
                        <li>
                            {{ link_to_route('fortress.logout', 'Logout'); }}
                        </li>
                    @else
                        @if (\Config::get('fortress::user.allow_registration'))
                            <li>{{ link_to_route('fortress.register', 'Register'); }}</li>
                        @endif
                        <li>{{ link_to_route('fortress.login', 'Login'); }}</li>
                    @endif
                </ul>
            </div>
        </div>
    </div>

    <div class="container">
        @section('body')
        @show
    </div>
</body>
</html>