{{ form_start($form) }}

{{ form_row($form->name) }}

<div class="form-group">
    <label>Permissions</label>

    <div class="checkbox" style="margin-top: 0;">
        @foreach ($permissions as $code => $permission)
            <label>
                {{ Form::checkbox('permissions[]', $code, isset($rolePermissions[$code])) }}
                <strong>{{ $code }}</strong> - {{ $permission }}
            </label><br>
        @endforeach
    </div>
</div>

{{ form_end($form) }}
