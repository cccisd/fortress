@extends('fortress::layouts.base')
@section('fortress.body')
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-6">
                <h1>Register</h1>
                <p>
                    {{ form($form) }}
                </p>
            </div>
        </div>
    </div>
@stop