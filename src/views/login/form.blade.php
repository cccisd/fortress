@extends('fortress::layouts.base')
@section('fortress.body')
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-6">
                <h1>Login</h1>
                <p>
                    {{ form($form) }}
                </p>

                @if (\Config::get('fortress::user.allow_registration'))
                    {{ link_to_route('fortress.register', 'Register') }}<br>
                @endif
                @if (\Config::get('fortress::user.allow_remind_password'))
                    <a href="#">Forgot your password?</a><br>
                @endif
            </div>
        </div>
    </div>
@stop