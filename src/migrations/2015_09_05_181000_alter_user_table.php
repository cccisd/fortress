<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fortress_users', function ($table) {
            $table->dropColumn('external_id');
            $table->dropColumn('permissions');
            $table->dropColumn('phone');
            $table->dropColumn('custom1');
            $table->dropColumn('custom2');
            $table->dropColumn('custom3');
            $table->dropColumn('custom4');
            $table->dropColumn('custom5');
            $table->boolean('is_super_user')->default(0);
            $table->softDeletes();
            DB::statement('ALTER TABLE `fortress_users` MODIFY `first_name` VARCHAR(255) NOT NULL;');
            DB::statement('ALTER TABLE `fortress_users` MODIFY `last_name` VARCHAR(255) NOT NULL;');
            DB::statement('ALTER TABLE `fortress_users` MODIFY `middle_name` VARCHAR(255) NOT NULL;');
            DB::statement('ALTER TABLE `fortress_users` MODIFY `username` VARCHAR(255) NULL;');

            $table->unique('email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fortress_users', function ($table) {
            $table->string('external_id')->nullable();
            $table->text('permissions')->nullable();
            $table->string('phone')->nullable();
            $table->string('custom1', 1024)->nullable();
            $table->string('custom2', 1024)->nullable();
            $table->string('custom3', 1024)->nullable();
            $table->string('custom4', 1024)->nullable();
            $table->string('custom5', 1024)->nullable();
            $table->dropColumn('is_super_user');
            $table->dropColumn('deleted_at');
        });
    }
}
