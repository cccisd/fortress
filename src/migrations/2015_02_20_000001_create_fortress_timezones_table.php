<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFortressTimezonesTable extends Migration
{
    use Cccisd\Fortress\Service\MigrationTrait;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fortress_timezones', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('country_code');
            $table->string('coordinates');
            $table->string('timezone');
            $table->string('description');

            $this->enforceConstraints($table);

            $table->index('country_code');
            $table->index('timezone');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fortress_timezones');
    }
}
