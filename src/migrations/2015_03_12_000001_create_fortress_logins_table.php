<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFortressLoginsTable extends Migration
{
    use Cccisd\Fortress\Service\MigrationTrait;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fortress_logins', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('fortress_user_id')->unsigned();
            $table->timestamp('login_at');
            $table->string('ip_address')->nullable();

            $this->enforceConstraints($table);

            $table->index('fortress_user_id');

            $table->
                foreign('fortress_user_id')->
                references('id')->
                on('fortress_users')
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fortress_logins');
    }
}
