<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveOldTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('fortress_groups_users');
        Schema::drop('fortress_groups');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('fortress_groups_users', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });

        Schema::create('fortress_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });
    }
}
