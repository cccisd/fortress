<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFortressThrottlesTable extends Migration
{
    use Cccisd\Fortress\Service\MigrationTrait;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fortress_throttles', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('fortress_user_id')->unsigned();
            $table->string('ip_address')->nullable();

            $table->integer('attempts')->default(0);
            $table->timestamp('last_attempt_at')->nullable();

            $table->boolean('suspended')->default(0);
            $table->timestamp('suspended_at')->nullable();

            $table->boolean('banned')->default(0);
            $table->timestamp('banned_at')->nullable();

            $this->enforceConstraints($table);

            $table->index('fortress_user_id');

            $table->
                foreign('fortress_user_id')->
                references('id')->
                on('fortress_users')
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fortress_throttles');
    }
}
