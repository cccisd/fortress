<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFortressGroupsUsersTable extends Migration
{
    use Cccisd\Fortress\Service\MigrationTrait;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fortress_groups_users', function (Blueprint $table) {
            $table->integer('fortress_user_id')->unsigned();
            $table->integer('fortress_group_id')->unsigned();
            $table->timestamps();

            $this->enforceConstraints($table);

            $table->primary(['fortress_user_id', 'fortress_group_id']);

            $table->
                foreign('fortress_user_id')->
                references('id')->
                on('fortress_users')
            ;
            $table->
                foreign('fortress_group_id')->
                references('id')->
                on('fortress_groups')
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fortress_groups_users');
    }
}
