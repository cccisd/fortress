<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFortressGroupsTable extends Migration
{
    use Cccisd\Fortress\Service\MigrationTrait;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fortress_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('external_id')->nullable();
            $table->string('name');
            $table->string('custom1', 1024)->nullable();
            $table->string('custom2', 1024)->nullable();
            $table->string('custom3', 1024)->nullable();
            $table->string('custom4', 1024)->nullable();
            $table->string('custom5', 1024)->nullable();
            $table->text('permissions')->nullable();
            $table->integer('parent_fortress_group_id')->unsigned()->nullable();

            $this->enforceConstraints($table);

            $table->unique('name');
            $table->
                foreign('parent_fortress_group_id')->
                references('id')->
                on('fortress_groups')
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fortress_groups');
    }
}
