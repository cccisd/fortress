<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRoleUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fortress_role_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fortress_role_id')->unsigned()->index();
            $table->foreign('fortress_role_id')->references('id')->on('fortress_roles')->onDelete('cascade');
            $table->integer('fortress_user_id')->unsigned()->index();
            $table->foreign('fortress_user_id')->references('id')->on('fortress_users')->onDelete('cascade');
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fortress_role_user');
    }
}
