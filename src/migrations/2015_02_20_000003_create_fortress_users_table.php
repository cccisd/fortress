<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFortressUsersTable extends Migration
{
    use Cccisd\Fortress\Service\MigrationTrait;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fortress_users', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('external_id')->nullable();

            $table->string('username');
            $table->string('password');
            $table->text('permissions')->nullable();

            $table->string('first_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->integer('fortress_timezone_id')->unsigned()->nullable();

            $table->string('custom1', 1024)->nullable();
            $table->string('custom2', 1024)->nullable();
            $table->string('custom3', 1024)->nullable();
            $table->string('custom4', 1024)->nullable();
            $table->string('custom5', 1024)->nullable();

            $table->boolean('activated')->default(0);
            $table->string('activation_code')->nullable();
            $table->timestamp('activated_at')->nullable();
            $table->string('persist_code')->nullable();
            $table->string('reset_password_code')->nullable();

            $this->enforceConstraints($table);

            $table->unique('username');
            $table->index('activation_code');
            $table->index('reset_password_code');
            $table->
                foreign('fortress_timezone_id')->
                references('id')->
                on('fortress_timezones')
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fortress_users');
    }
}
