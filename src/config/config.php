<?php

return [
    /**
     * Options related to routing.
     */
    'routes' => [
        /**
         * @var string prefix
         * All routes exposed in this package will lay underneath this prefix.
         * You might want to change this if there is a collision b/w this and
         * another package.
         */
        'prefix' => 'fortress',

        /**
         * @var string
         * The Url where to go after loging in
         */
        'after_login_url' => 'fortress/default',
    ],


    /**
     * Options related to session.
     */
    'session' => [
        /**
         * @var string prefix
         * All routes exposed in this package will lay underneath this prefix.
         * You might want to change this if there is a collision b/w this and
         * another package.
         */
        'key' => 'cccisd_fortress',
    ],


    /**
     * Options related to layout.
     */
    'layout' => [
        /**
         * @var string extends
         * If you use a Fortress view wholesale, this will be the layout that it
         * "@extends". Applications can update this to their specific layouts
         * for quick theming.
         */
        'extends' => 'fortress::layouts.default',

        /**
         * @var string container
         * From the layout given above, what "@section" shall Fortress content be
         * poured into?
         */
        'section' => 'body',
    ],


    /**
     * Options related to user.
     */
    'user' => [
        /**
         * Use activation process or not?
         */
        'use_activation' => false, // TODO: True option doesn't work right now. Don't change it.

        /**
         * Allow registration or not?
         */
        'allow_registration' => true,

        /**
         * Allow remind password or not?
         */
        'allow_remind_password' => false, // TODO: True option doesn't work right now. Don't change it.

        /**
         * You can override the login attribute without even subclassing the model,
         * simply by specifying the attribute below.
         * Options: 'username' or 'email'
         */
        'login_attribute' => 'username', // WARNING! You can change it ONLY when there are no users yet.

        /**
         * Maximum time in seconds for bulk upload execution
         */
        'bulk_upload_max_time' => 30,

        /**
         * List of possible permissions.
         */
        'permissions' => [
            'user.admin'          => 'Allow manage users',
            'user.changePassword' => 'Allow change user password',
            'user.bulkUpload'     => 'Allow bulk upload users',
            'user.loginAsUser'    => 'Allow login as another user',
            'role.admin'          => 'Allow manage roles',
        ],
    ],
];
