var React       = require('react');
var $           = require('jquery');
var Table       = require('cccisd-table');
var ModalForm   = require('cccisd-modal-form');
var ClickButton = require('cccisd-click-button');
var Loader      = require('cccisd-loader');
var Modal       = require('cccisd-modal');
var BulkUpload  = require('cccisd-bulk-upload');
var Fortress    = window.Fortress;

var UsersTab = React.createClass({
    url: {
        data: Fortress.baseUrl + 'admin/user',
        add: Fortress.baseUrl + 'admin/user/create',
        edit: Fortress.baseUrl + 'admin/user/{id}/edit',
        delete: Fortress.baseUrl + 'admin/user/{id}',
        changePassword: Fortress.baseUrl + 'admin/user/{id}/changePasswordForm',
        massAssignRole: Fortress.baseUrl + 'admin/user/massAssignRole',
        loginAsUser: Fortress.baseUrl + 'admin/user/loginAsUser/{id}',
    },
    getInitialState: function() {
        return {
            loading: true,
            data: [],
        };
    },
    componentDidMount() {
        this._updateData();
    },
    _updateData: function(loading = true) {
        var $this = this;

        $this.setState({
            loading: loading,
        });

        $.ajax({
            type: 'GET',
            url: $this.url.data,
            data: {},
            dataType: 'json',
            success(data) {
                $this.setState({
                    data: data,
                    loading: false,
                });
            },
        });
    },
    _getBulkUploadForm() {
        if (!Fortress.hasAccess('user.bulkUpload')) {
            return null;
        }

        return (
            <Modal
                trigger={
                    <button className="btn btn-primary">Bulk Upload</button>
                }
                title="User Bulk Upload"
                size="large"
                options={{backdrop: 'static'}}
            >
                <p>
                    CSV-file must have the first line with column names. The rest of the lines should be the data rows.<br />
                    This is the list of columns:<br />
                    <ul>
                        {Fortress.userBulkColumns.map(column => <li>{column}</li>)}
                    </ul>
                </p>
                <BulkUpload
                    url={Fortress.baseUrl + 'admin/user/bulkupload'}
                    onUpload={this._updateData.bind(this, false)}
                />
            </Modal>
        );
    },
    _getRowActions() {
        var rowActions = Fortress.roles.map(function(role) {
            return {text: 'Assign "' + role.name + '" role', action: this._assignRole, value: role.id};
        }.bind(this));

        return rowActions;
    },
    _assignRole(userList, roleId) {
        var $this = this;

        $this.setState({
            loading: true,
        });

        $.ajax({
            type: 'POST',
            url: $this.url.massAssignRole,
            data: {
                userList: userList,
                roleId: roleId,
            },
            dataType: 'json',
            success(data) {
                $this._updateData();
            },
        });
    },
    _getAddForm() {
        return (
            <ModalForm
                trigger={
                    <button className="btn btn-primary">
                        <span className="glyphicon glyphicon-plus" />
                        {' '}
                        Add user
                    </button>
                }
                title="Add user"
                url={this.url.add}
                onSuccess={this._updateData}
            />
        );
    },
    _getEditForm(id) {
        return (
            <ModalForm
                trigger={
                    <button className="btn btn-success btn-xs" title="Edit user" id={'edit_user_button_' + id}>
                        <span className="glyphicon glyphicon-pencil" />
                    </button>
                }
                title="Edit user"
                url={this.url.edit.replace('{id}', id)}
                modalWrapperClass="text-left"
                onSuccess={this._updateData}
            />
        );
    },
    _getChangePasswordForm(id) {
        var form = null;
        if (Fortress.hasAccess('user.changePassword')) {
            form = (
                <ModalForm
                    trigger={
                        <button className="btn btn-success btn-xs" title="Change password" id={'change_password_button_' + id}>
                            <span className="glyphicon glyphicon-lock" />
                        </button>
                    }
                    title="Change password"
                    url={this.url.changePassword.replace('{id}', id)}
                    modalWrapperClass="text-left"
                />
            );
        }

        return form;
    },
    _getLoginAsUserButton(id) {
        var button = null;
        if (Fortress.hasAccess('user.loginAsUser')) {
            button = (
                <a href={this.url.loginAsUser.replace('{id}', id)} className="btn btn-success btn-xs" title="Login as user" id={'login_as_user_button_' + id}>
                    <span className="glyphicon glyphicon-user" />
                </a>
            );
        }

        return button;
    },
    _getDeleteForm(id) {
        return (
            <ClickButton
                title={<span className="glyphicon glyphicon-remove" title="Delete user" />}
                className={'btn btn-xs btn-danger delete_user_button_' + id}
                isConfirm={true}
                onClick={this._deleteUser.bind(this, id)}
            />
        );
    },
    _deleteUser(id) {
        var $this = this;

        $this.setState({
            loading: true,
        });

        $.ajax({
            url: this.url.delete.replace('{id}', id),
            type: 'DELETE',
            success: function(result) {
                $this._updateData();
            },
        });
    },
    render: function() {
        var addForm = this._getAddForm();
        var bulkUploadForm = this._getBulkUploadForm();
        var rowActions = this._getRowActions();

        var columns = [
            {
                name: 'actions',
                label: 'Actions',
                class: 'text-center text-nowrap',
                render: function(value, row) {
                    var editForm = this._getEditForm(row.id);
                    var deleteForm = this._getDeleteForm(row.id);
                    var changePasswordForm = this._getChangePasswordForm(row.id);
                    var loginAsUserButton = this._getLoginAsUserButton(row.id);

                    return (
                        <span>
                            {editForm}
                            {' '}
                            {changePasswordForm}
                            {' '}
                            {loginAsUserButton}
                            {' '}
                            {deleteForm}
                        </span>
                    );
                }.bind(this),
            },
            {name: 'id', label: 'ID', class: 'text-right'},
            {name: 'username', label: 'Username', sort: true, filter: true},
            {name: 'email', label: 'Email', sort: true, filter: true},
            {
                name: 'name',
                label: 'Full name',
                sort: true,
                sortSettings: {field: ['first_name', 'last_name']},
                filter: true,
                filterSettings: {
                    type: 'text',
                    field: ['first_name', 'last_name'],
                },
                render: function(value, row) {
                    return row.first_name + ' ' + row.last_name;
                },
            },
            {
                name: 'roles',
                label: 'Roles',
                class: 'text-nowrap',
                render: function(value, row) {
                    return value.map(function(role) {
                        return <li key={role.id}>{role.name}</li>;
                    });
                },
                renderCSV: function(value, row) {
                    var roles = value.map(function(role) {
                        return role.name;
                    });

                    return roles.toString();
                },
            },
            {
                name: 'is_super_user',
                label: 'Superuser?',
                sort: true,
                filter: true,
                filterSettings: {
                    type: 'selectbox',
                    options: [
                        {label: 'yes', value: 1},
                        {label: 'no', value: 0},
                    ],
                },
                class: 'text-center',
                render: function(value, row) {
                    return value === 1 ? <span className="glyphicon glyphicon-ok"></span> : '';
                },
                renderCSV: function(value, row) {
                    return value === 1 ? 'yes' : '';
                },
            },
            {name: 'created_at', label: 'Created at', sort: true},
        ];

        return (
            <Loader loading={this.state.loading}>
                {addForm}{' '}{bulkUploadForm}

                <Table
                    data={this.state.data}
                    columns={columns}
                    rowActions={rowActions}
                    isRowNumber={true}
                    orderBy="created_at"
                    isAscentOrder={false}
                    isCsvDownload={true}
                />
            </Loader>
        );
    },
});

module.exports = UsersTab;
