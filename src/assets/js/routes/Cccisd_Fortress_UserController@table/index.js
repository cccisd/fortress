var React = require('react');
var $ = require('jquery');
var Tabs = require('cccisd-tabs');
var UsersTab = require('./usersTab');
var RolesTab = require('./rolesTab');
var Fortress = window.Fortress;

var Page = React.createClass({
    render: function() {
        var tabList = [];
        if (Fortress.hasAccess('user.admin')) {
            tabList.push({name: 'users', title: 'Users', content: <UsersTab />});
        }
        if (Fortress.hasAccess('role.admin')) {
            tabList.push({name: 'roles', title: 'Roles', content: <RolesTab />});
        }

        return (
            <div>
                <h2>User administration</h2>
                <Tabs
                    tabList={tabList}
                    saveInHash={true}
                />
            </div>
        );
    },
});


module.exports = function() {
    React.render(<Page />, $('.target')[0]);
};
