var React = require('react');
var $ = require('jquery');
var Table = require('cccisd-table');
var ModalForm = require('cccisd-modal-form');
var ClickButton = require('cccisd-click-button');
var Loader = require('cccisd-loader');
var Fortress = window.Fortress;


var RolesTab = React.createClass({
    url: {
        data: Fortress.baseUrl + 'admin/role',
        add: Fortress.baseUrl + 'admin/role/create',
        edit: Fortress.baseUrl + 'admin/role/{id}/edit',
        delete: Fortress.baseUrl + 'admin/role/{id}',
    },
    getInitialState: function() {
        return {
            loading: true,
            data: [],
        };
    },
    componentDidMount() {
        this._updateData();
    },
    _updateData: function() {
        var $this = this;

        $this.setState({
            loading: true,
        });

        $.ajax({
            type: 'GET',
            url: $this.url.data,
            data: {},
            dataType: 'json',
            success(data) {
                $this.setState({
                    data: data,
                    loading: false,
                });
            },
        });
    },
    _getAddForm() {
        return (
            <ModalForm
                trigger={
                    <button className="btn btn-primary">
                        <span className="glyphicon glyphicon-plus" />
                        {' '}
                        Add role
                    </button>
                }
                title="Add role"
                url={this.url.add}
                onSuccess={this._updateData}
            />
        );
    },
    _getEditForm(id) {
        return (
            <ModalForm
                trigger={
                    <button className="btn btn-success btn-xs" title="Edit">
                        <span className="glyphicon glyphicon-pencil" />
                    </button>
                }
                title="Edit role"
                url={this.url.edit.replace('{id}', id)}
                modalWrapperClass="text-left"
                onSuccess={this._updateData}
            />
        );
    },
    _getDeleteForm(id) {
        return (
            <ClickButton
                title={<span className="glyphicon glyphicon-remove" />}
                className="btn btn-xs btn-danger"
                isConfirm={true}
                onClick={this._deleteRole.bind(this, id)}
            />
        );
    },
    _deleteRole(id) {
        var $this = this;

        $this.setState({
            loading: true,
        });

        $.ajax({
            url: this.url.delete.replace('{id}', id),
            type: 'DELETE',
            success: function(result) {
                $this._updateData();
            },
        });
    },
    render: function() {
        var addForm = this._getAddForm();

        var columns = [
            {
                name: 'actions',
                label: 'Actions',
                class: 'text-center',
                render: function(value, row) {
                    var editForm = this._getEditForm(row.id);
                    var deleteForm = this._getDeleteForm(row.id);

                    return (
                        <span>
                            {editForm}{' '}{deleteForm}
                        </span>
                    );
                }.bind(this),
            },
            {name: 'id', label: 'ID', class: 'text-right'},
            {name: 'name', label: 'Name', sort: true, filter: true},
            {name: 'permissions', label: 'Permissions', render: function(value, row) {
                var result = [];
                for (var code in value) {
                    result.push(<li key={code}><strong>{code}</strong>{' - '}{value[code]}</li>);
                }

                return result;
            }},
            {name: 'created_at', label: 'Created at', sort: true},
        ];

        return (
            <Loader loading={this.state.loading}>
                {addForm}

                <Table
                    data={this.state.data}
                    columns={columns}
                    isRowNumber={true}
                />
            </Loader>
        );
    },
});

module.exports = RolesTab;
