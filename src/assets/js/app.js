var $ = require('jquery');

// Get the list of page-specific modules
var modules = require('./routes/*/index.js', {mode: 'hash', resolve: function(base, files, config) {
    var result = {};
    for (var key in files) {
        if (files.hasOwnProperty(key)) {
            var array = key.split('/');

            array.pop(); // remove the last element
            var route = array.pop();

            result[key] = route;
        }
    }

    return result;
}});


// Run the specific module for the page
$(document).ready(function() {
    var route = $('body').data('route').replace(/\\/g, '_');

    if (route !== '' && typeof modules[route] === 'function') {
        modules[route]();
    }
});
