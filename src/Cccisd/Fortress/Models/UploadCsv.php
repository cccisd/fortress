<?php namespace Cccisd\Fortress\Models;

class UploadCsv
{
    public $allowedColumns = [];
    public $excludedColumns = [];
    public $keyColumn = 'id';
    public $file = [];
    public $model;
    public $maximumTime = 30;

    protected $response;
    protected $header;
    protected $rows;
    protected $errorMessage = '';


    // Possible mime types for CSV
    protected $mimeTypes = [
        'text/comma-separated-values',
        'text/csv',
        'application/csv',
        'application/excel',
        'application/vnd.ms-excel',
        'application/vnd.msexcel',
        'text/anytext',
    ];


    protected function checkCsv()
    {
        if (!in_array($this->file['type'], $this->mimeTypes)) {
            return false;
        }

        return true;
    }


    protected function parseFile()
    {
        // It needs to be here for Mac
        ini_set('auto_detect_line_endings', true);

        // Parse CSV file into assoc array
        $rows = array_map('str_getcsv', file($this->file['tmp_name']));
        $this->header = array_map('strtolower', array_shift($rows));

        // Check if we know all the columns
        $diff = array_diff($this->header, $this->allowedColumns, $this->excludedColumns);
        if (count($diff) > 0) {
            $this->errorMessage = 'Unknown columns: "'.implode('", "', $diff).'"';
            return false;
        }

        $data = [];
        foreach ($rows as $row) {
            $data[] = array_combine($this->header, $row);
        }
        $this->rows = $data;

        return true;
    }


    public function process()
    {
        $response = [
            'success' => false,
            'error' => '',
            'header' => [],
            'count' => [
                'added' => 0,
                'updated' => 0,
                'failed' => 0,
            ],
            'rows' => [
                'added' => [],
                'updated' => [],
                'failed' => [],
            ],
        ];


        if (!$this->checkCsv()) {
            $response['error'] = 'Incorrect file type';
            return $response;
        }


        if (!$this->parseFile()) {
            $response['error'] = $this->errorMessage;
            return $response;
        }


        $startTime = \Carbon\Carbon::now()->getTimestamp();

        foreach ($this->rows as $row) {
            // Check if script is running for so long time
            $currentTime = \Carbon\Carbon::now()->getTimestamp();
            if ($currentTime - $startTime > $this->maximumTime) {
                $row['errors'] = 'Maximum execution time of '.$this->maximumTime.' seconds exceeded';
                $response['count']['failed']++;
                $response['rows']['failed'][] = $row;
                continue;
            }


            $isUpdating = false;

            // Decide if it's inserting or updating
            if (isset($row[$this->keyColumn])) {
                $model = $this->model->where($this->keyColumn, $row[$this->keyColumn])->first();
                if ($model) {
                    $isUpdating = true;
                } else {
                    $model = $this->model->newInstance();
                }
            } else {
                $model = $this->model->newInstance();
            }

            // Set attributes
            foreach ($row as $attr => $value) {
                if (!in_array($attr, $this->excludedColumns)) {
                    $model->{$attr} = $value;
                }
            }

            // Check by model validation rules
            if (!$model->save()) {
                $messages = [];
                foreach ($model->getErrors()->toArray() as $list) {
                    $messages = array_merge($messages, $list);
                }
                $row['errors'] = implode(' ', $messages);
                $response['count']['failed']++;
                $response['rows']['failed'][] = $row;
                continue;
            }

            if ($isUpdating) {
                $response['rows']['updated'][] = $row;
                $response['count']['updated']++;
            } else {
                $response['rows']['added'][] = $row;
                $response['count']['added']++;
            }
        }

        $response['header'] = array_merge($this->header, ['errors']);
        $response['success'] = true;

        return $response;
    }
}
