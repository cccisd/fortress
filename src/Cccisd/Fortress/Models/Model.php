<?php namespace Cccisd\Fortress\Models;

class Model extends \Eloquent
{
    /**
     * Error message bag
     *
     * @var Illuminate\Support\MessageBag
     */
    protected $errors;


    /**
     * Listen for save event
     */
    protected static function boot()
    {
        parent::boot();
        static::saving(function ($model) {
            return $model->validate();
        });
    }


    /**
     * Validation rules
     *
     * @var Array
     */
    public function rules($id = null)
    {
        return [];
    }


    /**
     * Custom messages
     *
     * @var Array
     */
    public function messages()
    {
        return [];
    }


    /**
     * Validates current attributes against rules
     */
    public function validate()
    {
        $v = \Validator::make($this->attributes, $this->rules($this->id), $this->messages());
        if ($v->passes()) {
            return true;
        }
        $this->setErrors($v->messages());
        return false;
    }


    /**
     * Set error message bag
     *
     * @var Illuminate\Support\MessageBag
     */
    protected function setErrors($errors)
    {
        $this->errors = $errors;
    }


    /**
     * Retrieve error message bag
     */
    public function getErrors()
    {
        return $this->errors;
    }


    /**
     * Inverse of wasSaved
     */
    public function hasErrors()
    {
        return !empty($this->errors);
    }
}
