<?php namespace Cccisd\Fortress\Models;

class Login extends \Eloquent
{
    protected $table = 'fortress_logins';

    public function record($fortress_user_id)
    {
        $this->fortress_user_id = $fortress_user_id;
        $this->login_at = $this->freshTimestamp();
        $this->ip_address = \Request::getClientIp();
        $this->save();
    }
}
