<?php namespace Cccisd\Fortress\Models;

class Role extends \Cccisd\Fortress\Models\Model
{
    protected $table = 'fortress_roles';

    protected $fillable = [
        'name',
    ];


    public function rules($id = null)
    {
        return [
            'name' => 'required|unique:'.$this->table.',name'.($id ? ",$id" : ''),
        ];
    }


    /**
     * Returns users for the role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('\Cccisd\Fortress\Models\User', 'fortress_role_user', 'fortress_role_id', 'fortress_user_id');
    }


    /**
     * An Accessor for Permissions
     * Return only possible permissions
     *
     * @return array
     */
    public function getPermissionsAttribute($value)
    {
        $permissions = \Config::get('fortress::user.permissions');
        $array = json_decode($value);

        $result = [];
        foreach ($array as $permission) {
            if (isset($permissions[$permission])) {
                $result[$permission] = $permissions[$permission];
            }
        }

        return $result;
    }
}
