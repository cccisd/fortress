<?php namespace Cccisd\Fortress\Models;

use Cccisd\Fortress\UserNotFoundException;
use Cccisd\Fortress\WrongPasswordException;
use Cccisd\Fortress\UserAlreadyActivatedException;

class User extends \Cccisd\Fortress\Models\Model
{
    use \SoftDeletingTrait;

    protected $table = 'fortress_users';

    protected $fillable = [
        'username',
        'email',
        'first_name',
        'last_name',
    ];

    // The login attribute.
    protected static $loginAttribute;

    // The user merged permissions.
    protected $mergedPermissions;


    public function rules($id = null)
    {
        $rules = [
            'username' => 'regex:/^([\d\w\.\-\_@])+$/|unique:'.$this->table.',username'.($id ? ",$id" : ''),
            'email' => 'email|unique:'.$this->table.',email'.($id ? ",$id" : ''),
            'password' => 'required',
            'activated' => 'boolean',
        ];

        // Make the login attribute required
        $rules[static::$loginAttribute] .= '|required';

        return $rules;
    }


    public function getBulkColumns()
    {
        $array = [
            'username',
            'email',
            'password',
            'first_name',
            'last_name',
        ];

        $all = \Schema::getColumnListing($this->getTable());
        $result = array_intersect($array, $all);

        return $result;
    }


    /**
     * Returns roles for the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany('Cccisd\Fortress\Models\Role', 'fortress_role_user', 'fortress_user_id', 'fortress_role_id');
    }


    public function setPasswordAttribute($pass)
    {
        $this->attributes['password'] = \Hash::make($pass);
    }


    /**
     * If Username is empty make it null
     * We need it just to not show unique error
     */
    public function setUsernameAttribute($value)
    {
        $this->attributes['username'] = $value == '' ? null : $value;
    }


    /**
     * If Email is empty make it null
     * We need it just to not show unique error
     */
    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = $value == '' ? null : $value;
    }


    /**
     * Finds a user by the given credentials.
     *
     * @param  array  $credentials
     * @return \Cartalyst\Sentry\Users\UserInterface
     */
    public function findByCredentials(array $credentials)
    {
        $user = $this->where(static::$loginAttribute, '=', $credentials[static::$loginAttribute])->first();

        if (!$user) {
            throw new UserNotFoundException('A user was not found with the given credentials.');
        }

        if (!\Hash::check($credentials['password'], $user->password)) {
            throw new WrongPasswordException('The password is incorrect');
        }

        return $user;
    }


    /**
     * Returns the user's ID.
     *
     * @return  mixed
     */
    public function getId()
    {
        return $this->getKey();
    }


    /**
     * Returns the user's login.
     *
     * @return mixed
     */
    public function getLogin()
    {
        return $this->{static::$loginAttribute};
    }


    /**
     * Gets a code for when the user is
     * persisted to a cookie or session which
     * identifies the user.
     *
     * @return string
     */
    public function getPersistCode()
    {
        $this->persist_code = str_random(42);
        $this->save();

        return $this->persist_code;
    }


    /**
     * Checks the given persist code.
     *
     * @param  string  $persistCode
     * @return bool
     */
    public function checkPersistCode($persistCode)
    {
        if (!$persistCode) {
            return false;
        }

        return $persistCode === $this->persist_code;
    }


    /**
     * Records a login for the user.
     *
     * @return void
     */
    public function recordLogin()
    {
        $login = new \Cccisd\Fortress\Models\Login;
        $login->record($this->getId());
    }


    /**
     * Get an activation code for the given user.
     *
     * @return string
     */
    public function getActivationCode()
    {
        $this->activation_code = $activationCode = str_random(42);
        $this->save();

        return $activationCode;
    }


    /**
     * Attempts to activate the given user by checking
     * the activate code. If the user is activated already,
     * an Exception is thrown.
     *
     * @param  string  $activationCode
     * @return bool
     */
    public function attemptActivation($activationCode)
    {
        if ($this->activated) {
            throw new UserAlreadyActivatedException('Cannot attempt activation on an already activated user.');
        }

        if ($activationCode == $this->activation_code) {
            $this->activation_code = null;
            $this->activated       = true;
            $this->activated_at    = $this->freshTimestamp();
            return $this->save();
        }

        return false;
    }


    /**
     * Check if the user is activated.
     *
     * @return bool
     */
    public function isActivated()
    {
        if (!\Config::get('fortress::user.use_activation')) {
            return true;
        }

        return (bool) $this->activated;
    }


    /**
     * See if a user has access to the passed permission(s).
     *
     * If multiple permissions are passed, the user must
     * have access to all permissions passed through.
     *
     * Super users have access no matter what.
     *
     * @param  string|array  $permissions
     * @return bool
     */
    public function hasAccess($permissions)
    {
        if ($this->isSuperUser()) {
            return true;
        }

        return $this->hasPermission($permissions);
    }


    /**
     * Returns if the user has access to any of the
     * given permissions.
     *
     * @param  array  $permissions
     * @return bool
     */
    public function hasAnyAccess(array $permissions)
    {
        if ($this->isSuperUser()) {
            return true;
        }

        return $this->hasPermission($permissions, false);
    }


    /**
     * Checks if the user is a super user - has
     * access to everything regardless of permissions.
     *
     * @return bool
     */
    public function isSuperUser()
    {
        return $this->is_super_user == 1;
    }


    /**
     * See if a user has access to the passed permission(s).
     *
     * If multiple permissions are passed, the user must
     * have access to all permissions passed through, unless the
     * "all" flag is set to false.
     *
     * @param  string|array  $permissions
     * @param  bool  $all
     * @return bool
     */
    public function hasPermission($permissions, $all = true)
    {
        $mergedPermissions = $this->getMergedPermissions();

        if ($all) {
            foreach ((array) $permissions as $permission) {
                if (!isset($mergedPermissions[$permission])) {
                    return false;
                }
            }

            return true;
        } else {
            foreach ((array) $permissions as $permission) {
                if (isset($mergedPermissions[$permission])) {
                    return true;
                }
            }

            return false;
        }
    }


    /**
     * Get the list of permission for current user
     * @return array
     */
    public function getActualPermissions()
    {
        // If superuser then return the whole list of permissions
        if ($this->isSuperUser()) {
            return \Config::get('fortress::user.permissions');
        }

        return $this->getMergedPermissions();
    }


    public function getMergedPermissions()
    {
        if (!$this->mergedPermissions) {
            $permissions = array();
            foreach ($this->roles()->get() as $role) {
                $permissions = array_merge($permissions, $role->permissions);
            }
            $this->mergedPermissions = $permissions;
        }

        return $this->mergedPermissions;
    }


    /**
     * Override the login attribute for all models instances.
     *
     * @param  string  $loginAttribute
     * @return void
     */
    public static function setLoginAttributeName($loginAttribute)
    {
        static::$loginAttribute = $loginAttribute;
    }


    /**
     * Get the current login attribute for all model instances.
     *
     * @return string
     */
    public static function getLoginAttributeName()
    {
        return static::$loginAttribute;
    }
}
