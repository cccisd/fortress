<?php namespace Cccisd\Fortress;

use Illuminate\Support\ServiceProvider;
use Cccisd\Fortress\Models\User;

class FortressServiceProvider extends ServiceProvider
{
    /**
     * Where to find the package code. Might be overloaded for testing
     * purposes.
     *
     * @var string $srcPath
     */
    public $srcPath;


    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;


    public function __construct()
    {
        $this->srcPath = __DIR__.'/../../';

        // Call parent constructor with the same parameters
        call_user_func_array(array('parent', '__construct'), func_get_args());
    }


    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->package('cccisd/fortress');

        // Load routes
        require $this->srcPath.'routes.php';

        // Load route filters
        require $this->srcPath.'filters.php';


        $loginAttribute = \Config::get('fortress::user.login_attribute');
        User::setLoginAttributeName($loginAttribute);
    }


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Register the service provider for dependencies
        $this->app->register('Kris\LaravelFormBuilder\FormBuilderServiceProvider');

        // Register the alias for dependencies
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $loader->alias('Fortress', 'Cccisd\Fortress\Facades\Fortress');
        $loader->alias('FormBuilder', 'Kris\LaravelFormBuilder\Facades\FormBuilder');

        $this->app->bindShared('fortress', function () {
            return new Fortress;
        });

        $this->registerCommands();
    }


    /**
     * Register Artisan commands
     *
     * @return void
     */
    public function registerCommands()
    {
        $this->app['createSuperuser'] = $this->app->share(function ($app) {
            return new \Cccisd\Fortress\Commands\CreateSuperuser;
        });

        $this->commands('createSuperuser');
    }


    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }
}
