<?php namespace Cccisd\Fortress;

use Cccisd\Fortress\LoginRequiredException;
use Cccisd\Fortress\UserNotActivatedException;

class Fortress
{
    protected $user;
    protected $role;
    protected $permission;

    protected $currentUser;
    protected $originalUser;
    protected $sessionKey;
    protected $loginAsUserKey = 'loginAsUser';


    public function __construct(\Cccisd\Fortress\Models\User $user = null)
    {
        $this->user = $user ?: new \Cccisd\Fortress\Models\User;

        $this->sessionKey = \Config::get('fortress::session.key');
    }


    /**
     * Registers a user by giving the required credentials
     * and an optional flag for whether to activate the user.
     *
     * @param  array  $credentials
     * @param  bool   $activate
     * @return \Cartalyst\Sentry\Users\UserInterface
     */
    public function register(array $credentials, $activate = false)
    {
        $user = $this->user->newInstance($credentials);
        $user->password = $credentials['password'];
        $user->save();

        if ($activate) {
            $user->attemptActivation($user->getActivationCode());
        }

        return $this->currentUser = $user;
    }


    /**
     * Attempts to authenticate the given user
     * according to the passed credentials.
     *
     * @param  array  $credentials
     * @param  bool   $remember
     */
    public function authenticate(array $credentials, $remember = false)
    {
        $user = $this->user->findByCredentials($credentials);
        $this->login($user, $remember);

        return $this->currentUser;
    }


    /**
     * Logs in the given user and sets properties in the session.
     *
     * @param  \Cartalyst\Sentry\Users\UserInterface $user
     * @param  bool  $remember
     * @return void
     */
    public function login(\Cccisd\Fortress\Models\User $user, $remember = false)
    {
        if (!$user->isActivated()) {
            $login = $user->getLogin();
            throw new UserNotActivatedException("Cannot login user [$login] as they are not activated.");
        }

        $this->currentUser = $user;

        // Create an array of data to persist to the session and / or cookie
        $toPersist = array($user->getId(), $user->getPersistCode());

        // Set sessions
        \Session::put($this->sessionKey, $toPersist);

        if ($remember) {
            $cookie = \Cookie::forever($this->sessionKey, $toPersist);
            \Cookie::queue($cookie);
        }

        // The user model can attach any handlers
        // to the "recordLogin" event.
        $user->recordLogin();
    }


    /**
     * Logs the current user out.
     *
     * @return void
     */
    public function logout()
    {
        $this->currentUser = null;
        \Session::flush();
        \Cookie::queue(\Cookie::forget($this->sessionKey));
    }


    /**
     * Check to see if the user is logged in and activated, and hasn't been banned or suspended.
     *
     * @return bool
     */
    public function check()
    {
        if (is_null($this->currentUser)) {
            // Check session first, follow by cookie
            if (!$userArray = \Session::get($this->sessionKey) and !$userArray = \Cookie::get($this->sessionKey)) {
                return false;
            }

            // Now check our user is an array with two elements,
            // the username followed by the persist code
            if (!is_array($userArray) or count($userArray) !== 2) {
                return false;
            }

            list($id, $persistCode) = $userArray;

            // Let's find our user
            try {
                $user = $this->user->find($id);
            } catch (UserNotFoundException $e) {
                return false;
            }

            if (!$user) {
                return false;
            }

            // Great! Let's check the session's persist code
            // against the user. If it fails, somebody has tampered
            // with the cookie / session data and we're not allowing
            // a login
            if (!$user->checkPersistCode($persistCode)) {
                return false;
            }


            // Save user in currentUser
            $this->currentUser = $user;


            // Check if the user is logged as some another user
            if ($loginAsUserId = \Session::get($this->loginAsUserKey)) {
                try {
                    $loginAsUser = $this->user->find($loginAsUserId);
                    $this->originalUser = $this->currentUser;
                    $user = $this->currentUser = $loginAsUser;
                } catch (UserNotFoundException $e) {
                    \Session::forget($this->loginAsUserKey);
                }
            }
        }

        // Let's check our cached user is indeed activated
        if (!$user = $this->getUser() or !$user->isActivated()) {
            return false;
        }

        return true;
    }


    /**
     * Put information about loginAsUser to the session
     *
     * @return
     */
    public function loginAsUser($loginAsUserId)
    {
        \Session::put($this->loginAsUserKey, $loginAsUserId);
    }


    /**
     * Remove session information about loginAsUser
     *
     * @return
     */
    public function logoutAsUser()
    {
        \Session::forget($this->loginAsUserKey);
    }


    /**
     * Returns the current user being used by Fortress, if any.
     *
     * @return
     */
    public function getUser()
    {
        // We will lazily attempt to load our user
        if (is_null($this->currentUser)) {
            $this->check();
        }

        return $this->currentUser;
    }


    /**
     * Returns the admin user who logged in as another user
     *
     * @return
     */
    public function getOriginalUser()
    {
        return $this->originalUser;
    }


    /**
     * Returns the user instance
     *
     * @return
     */
    public function getUserInstance(array $attributes = array())
    {
        return $this->user->newInstance($attributes);
    }


    public function getActualPermissions()
    {
        $user = $this->getUser();
        if (!$user) {
            return [];
        }

        return $user->getActualPermissions();
    }


    /**
     * Handle dynamic method calls into the method.
     *
     * @param  string  $method
     * @param  array   $parameters
     * @return mixed
     * @throws \BadMethodCallException
     */
    public function __call($method, $parameters)
    {
        $className = get_class($this);

        $userMethods = ['hasAccess', 'hasAnyAccess', 'isSuperUser'];
        if (in_array($method, $userMethods)) {
            $user = $this->getUser();

            if (!$user) {
                throw new LoginRequiredException('User has to be logged in');
            }

            return call_user_func_array([$user, $method], $parameters);
        }

        throw new \BadMethodCallException("Call to undefined method {$className}::{$method}()");
    }
}
