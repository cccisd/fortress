<?php namespace Cccisd\Fortress\Facades;

use Illuminate\Support\Facades\Facade;

class Fortress extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'fortress';
    }
}
