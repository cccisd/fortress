<?php
namespace Cccisd\Fortress\Service;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Schema\Grammars;

trait MigrationTrait
{
    protected function enforceConstraints(Blueprint $table)
    {
        $grammar = \Schema::getConnection()->getSchemaGrammar();
        if ($grammar instanceof Grammars\MySqlGrammar) {
            $table->engine = 'InnoDB';
        }
    }
}
