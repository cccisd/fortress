<?php namespace Cccisd\Fortress;

class UserNotFoundException extends \OutOfBoundsException {}
class WrongPasswordException extends UserNotFoundException {}
class UserAlreadyActivatedException extends \RuntimeException {}
class LoginRequiredException extends \UnexpectedValueException {}
class UserNotActivatedException extends \RuntimeException {}


// class LoginRequiredException extends \UnexpectedValueException {}
// class PasswordRequiredException extends \UnexpectedValueException {}
// class UserAlreadyActivatedException extends \RuntimeException {}
// class UserNotFoundException extends \OutOfBoundsException {}
// class UserNotActivatedException extends \RuntimeException {}
// class UserExistsException extends \UnexpectedValueException {}
// class WrongPasswordException extends UserNotFoundException {}