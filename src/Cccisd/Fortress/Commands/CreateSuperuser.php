<?php namespace Cccisd\Fortress\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CreateSuperuser extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'fortress:superuser';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Fortress superuser with all permissions.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $user = \Fortress::getUserInstance();
        $user->{$user::getLoginAttributeName()} = $this->argument('username');
        $user->password = $this->argument('password');
        $user->is_super_user = true;
        if (!$user->save()) {
            foreach ($user->getErrors()->toArray() as $errorList) {
                foreach ($errorList as $error) {
                    $this->error($error);
                }
            }
        } else {
            $user->attemptActivation($user->getActivationCode());
            $this->info('The user has been created');
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('username', InputArgument::REQUIRED, 'An username/email argument. Depends on the config settings.'),
            array('password', InputArgument::REQUIRED, 'An password argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }
}
