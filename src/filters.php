<?php

// Checks if the user is logged in
\Route::filter('auth', function () {
    if (!\Fortress::check()) {
        if (\Request::ajax()) {
            \App::abort(401);
        } else {
            // Save current url in session
            \Session::put('fortress.url', \Request::url());

            return \Redirect::route('fortress.login');
        }
    }
});


// Check if the user has all given permissions
\Route::filter('hasAccess', function ($route, $request) {
    $permissions = func_get_args();
    array_shift($permissions);
    array_shift($permissions);

    try {
        if (!\Fortress::hasAccess($permissions)) {
            \App::abort(403);
        }
    } catch (Cccisd\Fortress\LoginRequiredException $e) {
        if (\Request::ajax()) {
            \App::abort(401);
        } else {
            // Save current url in session
            \Session::put('fortress.url', \Request::url());

            return \Redirect::route('fortress.login');
        }
    }
});


// Check if the user has any of the given permissions
\Route::filter('hasAnyAccess', function ($route, $request) {
    $permissions = func_get_args();
    array_shift($permissions);
    array_shift($permissions);

    try {
        if (!\Fortress::hasAnyAccess($permissions)) {
            \App::abort(403);
        }
    } catch (Cccisd\Fortress\LoginRequiredException $e) {
        if (\Request::ajax()) {
            \App::abort(401);
        } else {
            // Save current url in session
            \Session::put('fortress.url', \Request::url());

            return \Redirect::route('fortress.login');
        }
    }
});
