<?php namespace Cccisd\Fortress;

use Illuminate\Support\MessageBag;

class LoginController extends \Controller
{
    public function form()
    {
        $loginAttribute = \Config::get('fortress::user.login_attribute');

        $form = \FormBuilder::plain(['method' => 'post', 'url' => route('fortress.login.save')])
            ->add($loginAttribute, 'text', ['label' => ucfirst($loginAttribute)])
            ->add('password', 'password', ['label' => 'Password'])
            ->add('remember_me', 'checkbox', ['label' => '&nbsp;Remember me'])
            ->add('submit', 'submit', [
                'label' => 'Log in',
                'attr' => ['class' => 'btn btn-primary'],
            ])
        ;

        return \View::make('fortress::login.form', compact('form'));
    }


    public function save()
    {
        $loginAttribute = \Config::get('fortress::user.login_attribute');

        $rules = [
            $loginAttribute => 'required',
            'password' => 'required',
        ];

        $validator = \Validator::make(\Input::all(), $rules);
        if ($validator->fails()) {
            return \Redirect::back()->withErrors($validator)->withInput();
        }

        try {
            \Fortress::authenticate(\Input::all(), \Input::get('remember_me'));
        } catch (\Exception $e) {
            $errors = new MessageBag([$loginAttribute => $e->getMessage()]);
            return \Redirect::back()->withErrors($errors)->withInput();
        }

        $url = \Session::pull('fortress.url', \Config::get('fortress::routes.after_login_url'));
        return \Redirect::to($url);
    }


    public function logout()
    {
        \Fortress::logout();

        return \Redirect::route('fortress.login');
    }
}
