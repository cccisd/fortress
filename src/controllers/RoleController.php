<?php namespace Cccisd\Fortress;

use Cccisd\Fortress\Models\Role;

class RoleController extends \Controller
{
    public function index()
    {
        $table = Role::all();

        return \Response::json($table);
    }


    /**
     * Show the new role form
     * It has to be AJAX request
     *
     * @return JSON
     */
    public function create()
    {
        $permissions = \Config::get('fortress::user.permissions');

        $form = \FormBuilder::plain(['method' => 'post', 'url' => route('fortress.admin.role.store')])
            ->add('name', 'text', ['label' => 'Name'])
            ->add('submit', 'submit', [
                'label' => 'Add role',
                'attr' => ['class' => 'btn btn-primary'],
            ])
        ;

        return \Response::json(array(
            'html' => \View::make('fortress::role.form', compact('form', 'permissions'))->render(),
        ));
    }


    /**
     * Save the new role
     *
     * @return JSON
     */
    public function store()
    {
        $role = new Role(\Input::all());
        $role->permissions = json_encode(\Input::get('permissions') ?: []);

        if (!$role->save()) {
            return \Response::json(array(
                'fail' => true,
                'errors' => $role->getErrors(),
            ));
        }

        return \Response::json(array(
            'success' => true,
        ));
    }


    /**
     * Show the new role form
     * It has to be AJAX request
     *
     * @return JSON
     */
    public function edit($id)
    {
        $role = Role::findOrFail($id);
        $permissions = \Config::get('fortress::user.permissions');
        $rolePermissions = $role->permissions;

        $form = \FormBuilder::plain(['method' => 'put', 'url' => route('fortress.admin.role.update', $id), 'model' => $role])
            ->add('name', 'text', ['label' => 'Name'])
            ->add('submit', 'submit', [
                'label' => 'Update role',
                'attr' => ['class' => 'btn btn-primary'],
            ])
        ;

        return \Response::json(array(
            'html' => \View::make('fortress::role.form', compact('form', 'permissions', 'rolePermissions'))->render(),
        ));
    }


    /**
     * Update the role
     *
     * @return JSON
     */
    public function update($id)
    {
        $role = Role::findOrFail($id);
        $role->permissions = json_encode(\Input::get('permissions') ?: []);
        $role->fill(\Input::all());

        if (!$role->save()) {
            return \Response::json(array(
                'fail' => true,
                'errors' => $role->getErrors(),
            ));
        }

        return \Response::json(array(
            'success' => true,
        ));
    }


    /**
     * Delete the role
     * @return JSON
     */
    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $role->delete();

        return \Response::json(array(
            'success' => true,
        ));
    }
}
