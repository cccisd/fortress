<?php namespace Cccisd\Fortress;

use Cccisd\Fortress\Models\Role;

class UserController extends \Controller
{
    public function defaultAction()
    {
        return \View::make('fortress::user.default');
    }


    public function table()
    {
        return \View::make('fortress::user.table');
    }


    /**
     * Return the list of users
     * @return JSON
     */
    public function index()
    {
        $table = \Fortress::getUserInstance()->with('roles')->get([
            'id',
            'username',
            'email',
            'first_name',
            'last_name',
            'is_super_user',
            'created_at',
        ]);

        return \Response::json($table);
    }


    /**
     * Show the new user form
     * It has to be AJAX request
     * @return JSON
     */
    public function create()
    {
        $roles = Role::lists('name', 'id');

        $form = \FormBuilder::plain([
            'method' => 'post',
            'url' => route('fortress.admin.user.store'),
            'id' => 'add_user_form',
        ])
            ->add('username', 'text', ['label' => 'Username'])
            ->add('email', 'text', ['label' => 'Email'])
            ->add('first_name', 'text', ['label' => 'First Name'])
            ->add('last_name', 'text', ['label' => 'Last Name'])
            ->add('password', 'password', ['label' => 'Password'])
            ->add('password_repeat', 'password', ['label' => 'Repeat password'])
            ->add('is_super_user', 'checkbox', ['label' => '&nbsp;Is super user?'])
            ->add('submit', 'submit', [
                'label' => 'Add user',
                'attr' => ['class' => 'btn btn-primary'],
            ])
        ;

        return \Response::json(array(
            'html' => \View::make('fortress::user.form', compact('form', 'roles'))->render(),
        ));
    }


    /**
     * Save the new user
     * @return JSON
     */
    public function store()
    {
        $rules = array_merge(\Fortress::getUserInstance()->rules(), [
            'password' => 'required|same:password_repeat',
        ]);

        $validator = \Validator::make(\Input::all(), $rules);
        if ($validator->fails()) {
            return \Response::json(array(
                'fail' => true,
                'errors' => $validator->getMessageBag()->toArray(),
            ));
        }

        // Add new user
        $user = \Fortress::getUserInstance(\Input::all());
        $user->password = \Input::get('password');

        // If you are superuser you can change superuser state
        if (\Fortress::isSuperUser()) {
            $user->is_super_user = \Input::get('is_super_user', 0);
        }

        $user->save();

        $user->roles()->sync(\Input::get('roles') ?: []);

        return \Response::json(array(
            'success' => true,
        ));
    }


    /**
     * Show the edit user form
     * It has to be AJAX request
     * @return JSON
     */
    public function edit($id)
    {
        $user = \Fortress::getUserInstance()->findOrFail($id);
        $roles = Role::lists('name', 'id');
        $userRoles = $user->roles->lists('name', 'id');

        $form = \FormBuilder::plain(['method' => 'put', 'url' => route('fortress.admin.user.update', $id), 'model' => $user])
            ->add('username', 'text', ['label' => 'Username'])
            ->add('email', 'text', ['label' => 'Email'])
            ->add('first_name', 'text', ['label' => 'First Name'])
            ->add('last_name', 'text', ['label' => 'Last Name'])
            ->add('is_super_user', 'checkbox', ['label' => '&nbsp;Is super user?'])
            ->add('submit', 'submit', [
                'label' => 'Update user',
                'attr' => ['class' => 'btn btn-primary'],
            ])
        ;

        return \Response::json(array(
            'html' => \View::make('fortress::user.form', compact('form', 'roles', 'userRoles'))->render(),
        ));
    }


    /**
     * Update the user
     * @return JSON
     */
    public function update($id)
    {
        $user = \Fortress::getUserInstance()->findOrFail($id);
        $user->fill(\Input::all());

        // If you are superuser you can change superuser state
        if (\Fortress::isSuperUser()) {
            $user->is_super_user = \Input::get('is_super_user', 0);
        }


        if (!$user->save()) {
            return \Response::json(array(
                'fail' => true,
                'errors' => $user->getErrors(),
            ));
        }

        $user->roles()->sync(\Input::get('roles') ?: []);

        return \Response::json(array(
            'success' => true,
        ));
    }


    /**
     * Delete the user
     * @return JSON
     */
    public function destroy($id)
    {
        $user = \Fortress::getUserInstance()->findOrFail($id);
        $user->delete();

        return \Response::json(array(
            'success' => true,
        ));
    }


    /**
     * Show the Change password form
     * It has to be AJAX request
     * @return JSON
     */
    public function changePasswordForm($id)
    {
        $form = \FormBuilder::plain(['method' => 'post', 'url' => route('fortress.admin.user.changePassword', $id)])
            ->add('password', 'password', ['label' => 'New password'])
            ->add('password_repeat', 'password', ['label' => 'Repeat password'])
            ->add('submit', 'submit', [
                'label' => 'Change password',
                'attr' => ['class' => 'btn btn-primary'],
            ])
        ;

        return \Response::json(array(
            'html' => form($form),
        ));
    }


    /**
     * Change password action
     * @return JSON
     */
    public function changePassword($id)
    {
        $user = \Fortress::getUserInstance()->findOrFail($id);

        $rules = [
            'password' => 'required|same:password_repeat',
        ];

        $validator = \Validator::make(\Input::all(), $rules);
        if ($validator->fails()) {
            return \Response::json(array(
                'fail' => true,
                'errors' => $validator->getMessageBag()->toArray(),
            ));
        }

        $user->password = \Input::get('password');
        if (!$user->save()) {
            return \Response::json(array(
                'fail' => true,
                'errors' => $user->getErrors(),
            ));
        }

        return \Response::json(array(
            'success' => true,
        ));
    }


    /**
     * Process the Bulk Upload action
     * It has to be AJAX request
     * @return JSON
     */
    public function bulkUpload()
    {
        $user = \Fortress::getUserInstance();

        $csv = new \Cccisd\Fortress\Models\UploadCsv;
        $csv->file = reset($_FILES);
        $csv->allowedColumns = $user->getBulkColumns();
        $csv->excludedColumns = ['errors'];
        $csv->model = $user;
        $csv->keyColumn = 'username';
        $csv->maximumTime = \Config::get('fortress::user.bulk_upload_max_time');

        $response = $csv->process();
        return \Response::json($response);
    }


    /**
     * Mass assign a role to many users
     * @return JSON
     */
    public function massAssignRole()
    {
        $roleId = \Input::get('roleId');
        $userList = \Input::get('userList');

        foreach ($userList as $userId) {
            $user = \Fortress::getUserInstance()->find($userId);
            if ($user) {
                $user->roles()->attach($roleId);
            }
        }

        return \Response::json(array(
            'success' => true,
        ));
    }


    /**
     * Login as another User
     * @param  int $id
     * @return redirect to default URL
     */
    public function loginAsUser($id)
    {
        \Fortress::loginAsUser($id);

        $url = \Config::get('fortress::routes.after_login_url');
        return \Redirect::to($url);
    }


    /**
     * Logout from another User
     * @return redirect to admin page
     */
    public function logoutAsUser()
    {
        \Fortress::logoutAsUser();

        return \Redirect::route('fortress.admin');
    }
}
