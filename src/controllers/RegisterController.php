<?php namespace Cccisd\Fortress;

class RegisterController extends \Controller
{
    public function form()
    {
        if (!\Config::get('fortress::user.allow_registration')) {
            \App::abort(403);
        }

        $loginAttribute = \Config::get('fortress::user.login_attribute');

        $form = \FormBuilder::plain(['method' => 'post', 'url' => route('fortress.register.save')])
            ->add($loginAttribute, 'text', ['label' => ucfirst($loginAttribute)])
            ->add('password', 'password', ['label' => 'Password'])
            ->add('password_repeat', 'password', ['label' => 'Repeat password'])
            ->add('submit', 'submit', [
                'label' => 'Register',
                'attr' => ['class' => 'btn btn-primary'],
            ])
        ;

        return \View::make('fortress::register.form', compact('form'));
    }


    public function save()
    {
        if (!\Config::get('fortress::user.allow_registration')) {
            \App::abort(403);
        }

        $rules = array_merge(\Fortress::getUserInstance()->rules(), [
            'password' => 'required|same:password_repeat',
        ]);


        $validator = \Validator::make(\Input::all(), $rules);
        if ($validator->fails()) {
            return \Redirect::back()->withErrors($validator)->withInput();
        }

        // Register user
        \Fortress::register(\Input::all(), true);

        // Authenticate user
        \Fortress::authenticate(\Input::all());

        $url = \Config::get('fortress::routes.after_login_url');
        return \Redirect::to($url);
    }
}
