<?php

/*
|--------------------------------------------------------------------------
| Fortress Routes
|--------------------------------------------------------------------------
*/


Route::group(['prefix' => Config::get('fortress::routes.prefix')], function () {
    // Registration
    \Route::get('register', [
        'as' => 'fortress.register',
        'uses' => 'Cccisd\Fortress\RegisterController@form',
    ]);
    \Route::post('register', [
        'as' => 'fortress.register.save',
        'uses' => 'Cccisd\Fortress\RegisterController@save',
    ]);


    // Login
    \Route::get('login', [
        'as' => 'fortress.login',
        'uses' => 'Cccisd\Fortress\LoginController@form',
    ]);
    \Route::post('login', [
        'as' => 'fortress.login.save',
        'uses' => 'Cccisd\Fortress\LoginController@save',
    ]);
    \Route::get('logout', [
        'as' => 'fortress.logout',
        'uses' => 'Cccisd\Fortress\LoginController@logout',
    ]);
    \Route::get('logoutAsUser', [
        'as' => 'fortress.admin.user.logoutAsUser',
        'uses' => 'Cccisd\Fortress\UserController@logoutAsUser',
    ]);


    // Manage users and groups
    \Route::group(['before' => 'auth'], function () {
        \Route::get('default', [
            'as' => 'fortress.default',
            'uses' => 'Cccisd\Fortress\UserController@defaultAction',
        ]);

        \Route::group(['prefix' => 'admin'], function () {
            \Route::get('/', [
                'before' => 'hasAnyAccess:user.admin,role.admin',
                'as' => 'fortress.admin',
                'uses' => 'Cccisd\Fortress\UserController@table',
            ]);

            \Route::group(['before' => 'hasAccess:user.admin'], function () {
                // Manage users
                \Route::resource('user', 'Cccisd\Fortress\UserController', [
                    'only' => ['index', 'create', 'store', 'edit', 'update', 'destroy'],
                ]);
                \Route::get('user/{id}/changePasswordForm', [
                    'before' => 'hasAccess:user.changePassword',
                    'as' => 'fortress.admin.user.changePasswordForm',
                    'uses' => 'Cccisd\Fortress\UserController@changePasswordForm',
                ]);
                \Route::post('user/{id}/changePassword', [
                    'before' => 'hasAccess:user.changePassword',
                    'as' => 'fortress.admin.user.changePassword',
                    'uses' => 'Cccisd\Fortress\UserController@changePassword',
                ]);
                \Route::post('user/bulkupload', [
                    'before' => 'hasAccess:user.bulkUpload',
                    'as' => 'fortress.admin.user.bulkUpload',
                    'uses' => 'Cccisd\Fortress\UserController@bulkUpload',
                ]);
                \Route::post('user/massAssignRole', [
                    'as' => 'fortress.admin.user.massAssignRole',
                    'uses' => 'Cccisd\Fortress\UserController@massAssignRole',
                ]);
                \Route::get('user/loginAsUser/{id}', [
                    'before' => 'hasAccess:user.loginAsUser',
                    'as' => 'fortress.admin.user.loginAsUser',
                    'uses' => 'Cccisd\Fortress\UserController@loginAsUser',
                ]);
            });

            \Route::group(['before' => 'hasAccess:role.admin'], function () {
                // Manage roles
                \Route::resource('role', 'Cccisd\Fortress\RoleController', [
                    'only' => ['index', 'create', 'store', 'edit', 'update', 'destroy'],
                ]);
            });
        });
    });
});
